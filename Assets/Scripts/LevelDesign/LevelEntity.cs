using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//[Serializable] 
//public class LevelEntity  
//{
//    public int Level;
//    public List<CharacterEntity> _characters;
//    public List<ToiletEntity> _toiletEntity;
//    public List<ObstacleEntity> _obstacle; 
//}
public class LevelEntity : MonoBehaviour
{
    [SerializeField]
    public LevelData levelData;
    public void AddToLevelScriptableObject(LevelScriptableObject levelScriptableObject)
    {
        if (levelScriptableObject != null)
        {
            levelScriptableObject.levelData.Add(levelData);
            UnityEditor.EditorUtility.SetDirty(levelScriptableObject);
        }
    }
}

[Serializable]
public class LevelData
{
    public int Level;
    public List<CharacterEntity> _characters;
    public List<ToiletEntity> _toiletEntity;
    public List<ObstacleEntity> _obstacle;
}
[Serializable]
public class CharacterEntity
{
    public GenderType GenderType;
    public Vector2 Position;
}/// <summary>
/// Quaternion rotate = Quaternion.Euler(new Vector3(toiletEntity.Rotate.x, toiletEntity.Rotate.y, 0f));
/// </summary>
[Serializable]
public class ObstacleEntity
{
    public TypeObstacle TypeObstacle;
    public Vector2 Position;
    public Vector3 Rotate;
}
[Serializable]
public class ToiletEntity
{
    public GenderType GenderType;
    public Vector2 Position;
   
}
