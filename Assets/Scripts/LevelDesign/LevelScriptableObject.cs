using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/LeveConfig", order = 1)]
public class LevelScriptableObject : ScriptableObject
{
    public List<LevelData> levelData = new List<LevelData>();
}


