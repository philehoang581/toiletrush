using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacles : MonoBehaviour
{
    [SerializeField] private TypeObstacle typeObstacle;
    private ObstacleEntity obstacleEntity;

    public ObstacleEntity ObstacleEntity { set; get; }

    public TypeObstacle TypeObstacle
    {
        get { return typeObstacle; }
    }

}
