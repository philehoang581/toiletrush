using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Toilet : MonoBehaviour
{
    private ToiletEntity _toiletEntity;

    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private Sprite _spriteMen;
    [SerializeField] private Sprite _spriteWomen;
    private Action<Toilet> _actionTrigger;
    //private Type _type;
    private bool _isTrigger = false;
    private Vector3 _position;
    [SerializeField] private GenderType _genderType;
  


    public ToiletEntity ToiletEntity
    {
        set { _toiletEntity = value; }
        get { return _toiletEntity; }
    }

    public GenderType GenderType
    {
        get
        {
            return _genderType;
        }
        set
        {
            _genderType = value;
        }
    }
    public bool IsTrigger
    {
        get
        {
            return _isTrigger;
        }
    }
    public void Inint(ToiletEntity toiletEntity, Action<Toilet> action)
    {
        this.transform.position = toiletEntity.Position;
        _actionTrigger = action;
        _genderType = toiletEntity.GenderType;
        switch (_genderType)
        {
            case GenderType.Men:
                _spriteRenderer.sprite = _spriteMen;
                break;
            case GenderType.Women:
                _spriteRenderer.sprite = _spriteWomen;
                break;
            default:
                Debug.LogError($"{_genderType}");
                break;

        }
    }
    //void OnTriggerEnter2D(Collider2D collider)
    //{
    //    if (collider.tag == "Player_Men" || collider.tag == "Player_Women")
    //    {
    //        // Do something when the player enters the trigger
    //        Debug.Log($"Toilet OnTriggerEnter2D: {collider.name}");
    //        if(_actionTrigger!=null) _actionTrigger.Invoke();                                                                                                               
    //    }
    //}
    //private void OnMouseUp()
    //{
    //    Debug.Log($"OnMouseUp: {this.name}");
    //}
    //private void OnMouseUpAsButton()
    //{
    //    Debug.Log($"OnMouseUp: {this.name}");
    //}
    private void OnMouseExit()
    {
        //Debug.LogError($"OnMouseExit: {this.name}");
        _isTrigger = false;
        if (_actionTrigger != null) _actionTrigger.Invoke(this);
    }
    private void OnMouseEnter()
    {
        _isTrigger = true;
        //Debug.LogError($"OnMouseEnter: {this.name}");
        if (_actionTrigger != null) _actionTrigger.Invoke(this);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player_Men") || collision.gameObject.CompareTag("Player_Women"))
        {
            // Do something when the player enters the trigger
            Debug.Log($"Toilet OnTriggerEnter2D: {collision.gameObject.name}");
            if (_actionTrigger != null) _actionTrigger.Invoke(this);
        }
    }
}
