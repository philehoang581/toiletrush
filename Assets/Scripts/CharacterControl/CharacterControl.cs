using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class CharacterControl : MonoBehaviour
{
    public CharacterEntity CharacterEntity;

    [SerializeField] private Transform _characterTransform;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private Sprite _spriteMen;
    [SerializeField] private Sprite _spriteWomen;

    private List<Vector3> _listPathMove;
    
    private Action<CharacterControl> _actionTrigger; 
    private bool _alreadyDrawn = true;
    private bool _isTrigger = false;
    private bool _isCompletMove = false;
    private Collider2D _collider2D;
    public bool AlreadyDrawn
    {
        get
        {
            return _alreadyDrawn;
        }
    }
    public bool IsTrigger
    {
        get
        {
            return _isTrigger;
        }
    }
    public bool IsCompletMove
    {
        get
        {
            return _isCompletMove;
        }
        set
        {
            _isCompletMove = value;
        }
    }
    public Collider2D Collision2D
    {
        get
        {
            return _collider2D;
        }
    }
    public void Init(CharacterEntity characterEntity, Action<CharacterControl> actionOnSelect)
    {
        CharacterEntity = characterEntity;
        this.transform.position = characterEntity.Position;
       // _actionComplet = actionComplet;
        _actionTrigger = actionOnSelect;

        switch (characterEntity.GenderType)
        {
            case GenderType.Men:
                _spriteRenderer.sprite = _spriteMen;
                break;
            case GenderType.Women:
                _spriteRenderer.sprite = _spriteWomen;
                break;
            default:
                Debug.LogError($"{characterEntity.GenderType}");
                break;

        }
    }
    public void Move()
    {
        if (_listPathMove == null || _listPathMove.Count <= 0)
        {
            Debug.LogError($"SmoothMove NULL LIST PATH !!!!!!!!!!!!!!");
            return;
        }
       // _listPathMove = listPoint;
        StartCoroutine(SmoothMove(_listPathMove)); 
    }
    public void StopMove()
    {
        StopAllCoroutines();
    }
    public void SetPathMove(List<Vector3> listPoint)
    {
        if (listPoint == null || listPoint.Count <= 0)
        {
            Debug.LogError($"SmoothMove NULL LIST PATH !!!!!!!!!!!!!!");
            return;
        }
        _listPathMove = listPoint;
        //StartCoroutine(SmoothMove(listPoint));
        _alreadyDrawn = false;
    }
    private IEnumerator SmoothMove(List<Vector3> listPoint)
    {
        
        for (int i = 0; i < listPoint.Count; i++)
        {
            yield return new WaitForEndOfFrame();
            //Debug.LogError($"SmoothMove: , {listPoint[i]}");
            _characterTransform.position = listPoint[i]; 
        }
        //if (_actionComplet != null) _actionComplet.Invoke(false);
    }
    private void OnMouseDown()
    { 
        if (_actionTrigger != null)
        {
            _actionTrigger.Invoke(this);
        } 
    } 
    private void OnTriggerEnter2D(Collider2D collider2D)
    { 
        _isTrigger = true;
        _collider2D = collider2D; 
        if (_actionTrigger != null)
        {
            _actionTrigger.Invoke(this);
        }
    } 
}
