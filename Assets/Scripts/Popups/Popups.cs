using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Popups : MonoBehaviour
{ 
    [SerializeField] private GameObject _panel;  
    public CanvasGroup Root; 
    #region Canvas Get
    static GameObject _popup;
    public static GameObject Popup
    {
        get
        {
            if (_popup == null)
                _popup = GameObject.FindWithTag("Popup");

            return _popup;
        }
    }
     
    static GameObject _canvasToast;
    public static GameObject CanvasToast
    {
        get
        {
            if (_canvasToast == null)
                _canvasToast = GameObject.FindWithTag("Canvas Toast");

            return _canvasToast;
        }
    }

    static GameObject _canvasFX;
    public static GameObject CanvasFX
    {
        get
        {
            if (_canvasFX == null)
                _canvasFX = GameObject.FindWithTag("Canvas FX");

            return _canvasFX;
        }
    }
    #endregion
     
    public virtual void Appear()
    { 
        Root.alpha = 1;
        Root.interactable = true;
        Root.blocksRaycasts = true; 
    }

    public virtual void Disappear()
    { 
        Root.alpha = 0;
        Root.interactable = false;
        Root.blocksRaycasts = false; 
    }

    public virtual void Disable()
    {
        Root.alpha = 0;
        Root.interactable = false;
        Root.blocksRaycasts = false; 
    } 
}
