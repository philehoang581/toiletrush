using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PopupFail : Popups
{
    public static PopupFail _instance;
    private Action _actionReplay;
    public static void Show(Action actionReplay)
    {
        CheckInstance();
        _instance._actionReplay = actionReplay;
        _instance.Appear();
        _instance.Init();
    }
    static void CheckInstance()
    {
        if (_instance == null)
        {
            _instance = Instantiate(
                Resources.Load<PopupFail>("Popups/PopupFail"),
                Popups.Popup.transform,
                false);
        }
    }
    private void Init()
    {
         
    }
    public void PlayAgain()
    {
        _instance._actionReplay?.Invoke();
        Disappear();
    }
}
