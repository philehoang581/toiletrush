using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PopupWin : Popups
{
    public static PopupWin _instance;
    private Action _actionNextLevel;
    public static void Show(Action actionNextLevel)
    {
        CheckInstance();  
        _instance._actionNextLevel = actionNextLevel;
    }
    static void CheckInstance()
    {
        if (_instance == null)
        {
            _instance = Instantiate(
                Resources.Load<PopupWin>("Popups/PopupWin"),
                Popups.Popup.transform,
                false);
        }

        if (_instance != null)
        {
            _instance.Appear();
            _instance.Init();
        }
        else
        {
            Debug.LogError($"PopupWin _instance == null");
        } 
    } 
    private void Init()
    {

    }
    public void NextLevel()
    {
        _instance._actionNextLevel?.Invoke();
        Disappear();
    }
}
