using UnityEngine;
using System.Collections.Generic;

public class LineDrawer : MonoBehaviour
{ 
    [SerializeField] private LineRenderer _lineRenderer; 
    List<Vector3> points = new List<Vector3>();
    private bool _isHaDrew;
     
    public void Init(List<Vector3> listPoint)
    { 
        _lineRenderer.positionCount = listPoint.Count;
        _lineRenderer.SetPositions(listPoint.ToArray());
    } 
}