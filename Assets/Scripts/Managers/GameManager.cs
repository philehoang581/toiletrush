using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    [SerializeField] private LevelScriptableObject _configGame;
    [SerializeField] private Toilet _prefabToilet;
    [SerializeField] private Obstacles _prefabObstacle;
    [SerializeField] private CharacterControl _prefabCharacter;
    [SerializeField] private LineRenderer _lineRenderer; 
    private CharacterControl _currentCharacter;
    private List<Vector3> _points = new List<Vector3>();
    private List<CharacterControl> _listCharacter = new List<CharacterControl>();
    private bool _isCanDraw;
    private bool _isTarget;  
    private bool _isRecording = false;
    private bool _isHasMove = false;
    private DrawingState _drawingState;

    private static int _level = 0; 

    private void Start()
    { 
        InitGame(); 
    }
    
    private void InitGame()
    {
        LevelData levelData = GetLevelData(_level);
        if (levelData == null)
        {
            Debug.LogError($"Level: {_level} data null");
            return;
        }
        //Init Character
        for (int i = 0; i < levelData._characters.Count; i++)
        {
            //var character = _charcterPool.GetObject();
            var character = Instantiate(_prefabCharacter);
            character.Init(levelData._characters[i], OnSelectCharacter);
            _listCharacter.Add(character);
        }

        //Init Toilet
        for (int i = 0; i < levelData._toiletEntity.Count; i++)
        {
            var toilet = Instantiate(_prefabToilet);
            toilet.Inint(levelData._toiletEntity[i], TriggerToilet); 
        }
        //Init Obstacles
        for (int i = 0; i < levelData._obstacle.Count; i++)
        {
            var obstacleEntity = levelData._obstacle[i];
            var obstacle = Instantiate(_prefabObstacle);

            Quaternion rotate = Quaternion.Euler(new Vector3(obstacleEntity.Rotate.x, obstacleEntity.Rotate.y, obstacleEntity.Rotate.z));
            obstacle.transform.rotation = rotate;
            obstacle.transform.position = obstacleEntity.Position; 
        }
    }
    private LevelData GetLevelData(int level)
    {
        if (level >= _configGame.levelData.Count)
        {
            Debug.LogError("Invalid level requested: " + level);
            return null;
        }

        return _configGame.levelData[level];
    }

    private void TriggerToilet(Toilet toilet)
    {
        if (_currentCharacter == null) return;
        if (_currentCharacter.CharacterEntity.GenderType == toilet.GenderType)
        {
            _isTarget = toilet.IsTrigger;
        }
        else
        {
            _isTarget = false;
        } 
    }
    private void OnSelectCharacter(CharacterControl characterControl)
    {
        if (characterControl.IsTrigger)
        {

            if (characterControl.Collision2D != null && characterControl.Collision2D.tag == "Toilet")
            {
                characterControl.IsCompletMove = true;

                var tempCharacter = _listCharacter.Find(x => x.IsCompletMove == false);
                if (tempCharacter == null)
                    PassLevel();
            }
            else
            {
                ShowPopupReplay();
            }
        }
        else
        {
            _isCanDraw = characterControl.AlreadyDrawn;
            _currentCharacter = characterControl;
        } 
    }
    
    void Update()
    {
        switch (_drawingState)
        {
            case DrawingState.None:
                CheckStartDrawing();
                break;
            case DrawingState.Drawing:
               // RecordDrawing();
                break;
            case DrawingState.Finished:
                //FinishDrawing();
                break;
        } 
        if (Input.GetMouseButtonUp(0))
        {
            _isRecording = false;
            _drawingState = DrawingState.None;
            if (_isTarget)
            {
                List<Vector3> path = new List<Vector3>(_points);
                var ob = Instantiate(_lineRenderer); 
                if (_currentCharacter.CharacterEntity.GenderType == GenderType.Men)
                {
                    ob.material.color = Color.blue;
                }
                else
                {
                    ob.material.color = Color.red;
                }
                ob.positionCount = path.Count;
                ob.SetPositions(path.ToArray());

                _currentCharacter.SetPathMove(path);
            }
            else
            {

            }
            _isCanDraw = false;
            _points.Clear();
            _lineRenderer.positionCount = 0;
        }
        if (_isRecording) CreateLineRenderer(); 
        if(AllCharactersHaveDrawn()) MoveAllCharacters();
    }

    private void CheckStartDrawing()
    {
        if (Input.GetMouseButtonDown(0) && _isCanDraw)
        {
            _drawingState = DrawingState.Drawing;
            _isRecording = true;
        }
    }
    private void CreateLineRenderer()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.z = 0;
        if (!_points.Contains(mousePosition)) _points.Add(mousePosition);
        if (_currentCharacter.CharacterEntity.GenderType == GenderType.Men)
        {
            _lineRenderer.material.color = Color.blue;
        }
        else
        {
            _lineRenderer.material.color = Color.red;
        }
        _lineRenderer.positionCount = _points.Count;
        _lineRenderer.SetPositions(_points.ToArray());
    }
    private bool AllCharactersHaveDrawn()
    {
        return _listCharacter.Find(x => x.AlreadyDrawn == true) == null;
    }
     
    private void MoveAllCharacters()
    {
        if (_isHasMove == true) return;
        for (int i = 0; i < _listCharacter.Count; i++)
        {
            _listCharacter[i].Move();
        }
        _isHasMove = true;
    }

    private void PassLevel()
    {
        Debug.LogError($"PASS LEVEL!!!");
        PopupWin.Show(()=>
        {
            _level++;
            SceneManager.LoadScene("gameplay");
        });
    } 

    private void ShowPopupReplay()
    {
        Debug.LogError($"ShowPopupReplay!!!"); 
        _listCharacter.ForEach(x=>x.StopAllCoroutines());
        PopupFail.Show(()=>
        { 
            SceneManager.LoadScene("gameplay");
        }); 
    } 
}
