 
public enum GenderType 
{ 
    Men,
    Women
}
public enum TypeObstacle
{
    Wall,
    Barrier,
    Car,
    Monster, 
    Box
}
public enum DrawingState
{
    None,
    Drawing,
    Finished
}
