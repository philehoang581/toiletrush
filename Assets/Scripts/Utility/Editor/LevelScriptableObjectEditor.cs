using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(LevelScriptableObject))]
public class LevelScriptableObjectEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Add Level"))
        {
            //LevelScriptableObject levelConfig = (LevelScriptableObject)target;

            //// Create a new LevelEntity and copy its LevelData
            //GameObject levelObject = new GameObject("LevelEntity");
            //LevelEntity levelEntity = levelObject.AddComponent<LevelEntity>();
            //levelEntity.levelData = new LevelData();
            //// Copy the data from the scene objects to the LevelData
            //levelEntity.levelData.Level = levelConfig.levelData.Count + 1;
            //levelEntity.levelData._characters = new List<CharacterEntity>(FindObjectsOfType<CharacterEntity>());
            //levelEntity.levelData._toiletEntity = new List<ToiletEntity>(FindObjectsOfType<ToiletEntity>());
            //levelEntity.levelData._obstacle = new List<ObstacleEntity>(FindObjectsOfType<ObstacleEntity>());

            //levelEntity.AddToLevelScriptableObject(levelConfig);

            //LevelScriptableObject levelConfig = AssetDatabase.LoadAssetAtPath<LevelScriptableObject>("Assets/ConfigGame/LevelConfig.asset");
            LevelScriptableObject levelConfig = AssetDatabase.LoadAssetAtPath<LevelScriptableObject>("Assets/Data/LevelConfig.asset");

            if (levelConfig == null)
            {
                levelConfig = ScriptableObject.CreateInstance<LevelScriptableObject>();
                AssetDatabase.CreateAsset(levelConfig, "Assets/Data/LevelConfig.asset");
                AssetDatabase.SaveAssets();
            }

            LevelData levelData = new LevelData();
            levelData.Level = levelConfig.levelData.Count + 1;

            // Add CharacterEntities to levelData
            CharacterControl[] characterControls = FindObjectsOfType<CharacterControl>();
            levelData._characters = new List<CharacterEntity>();
            foreach (CharacterControl characterControl in characterControls)
            {
                CharacterEntity characterEntity = new CharacterEntity();
                characterEntity.GenderType = characterControl.CharacterEntity.GenderType;
                characterEntity.Position = characterControl.transform.position;
                levelData._characters.Add(characterEntity);
            }

            // Add ToiletEntities and ObstacleEntities to levelData

            // Add ToiletEntities to levelData
            Toilet[] toilets = FindObjectsOfType<Toilet>();
            levelData._toiletEntity = new List<ToiletEntity>();
            foreach (Toilet toilet in toilets)
            {
                ToiletEntity toiletEntity = new ToiletEntity();
                toiletEntity.GenderType = toilet.GenderType;
                toiletEntity.Position = toilet.transform.position;
                

                levelData._toiletEntity.Add(toiletEntity);
            }

            // Add ObstacleEntities to levelData
            Obstacles[] obstacles = FindObjectsOfType<Obstacles>();
            levelData._obstacle = new List<ObstacleEntity>();
            foreach (Obstacles obstacle in obstacles)
            {
                ObstacleEntity obstacleEntity = new ObstacleEntity();  
                obstacleEntity.Position = obstacle.transform.position;
                obstacleEntity.TypeObstacle = obstacle.TypeObstacle;  
                Vector3 rotate = new Vector3(obstacle.transform.rotation.eulerAngles.x, obstacle.transform.rotation.eulerAngles.y, 
                    obstacle.transform.rotation.eulerAngles.z); 
                obstacleEntity.Rotate = rotate; 
                levelData._obstacle.Add(obstacleEntity);
            }


            levelConfig.levelData.Add(levelData);
            EditorUtility.SetDirty(levelConfig);
        }
    }
}
